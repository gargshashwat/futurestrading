import os
import pandas as pd

from kiteconnect import KiteConnect

import config as cfg


def get_data_from_kite(kite, token, start_date, end_date):
    """Get continuous OHLCV daily data from kite and return as a dataframe"""
    ret = []
    freq = 2000  # kite can only output 2000 days at a time
    for s in pd.date_range(start_date, end_date, freq=f'{freq}D'):
        e = min(s + pd.Timedelta(freq-1, unit='d'), pd.to_datetime(end_date))        
        data = kite.historical_data(instrument_token=token, from_date=str(s.date()), to_date=str(e.date()), interval='day', continuous=True)
        ret += data
    df = pd.DataFrame(ret)
    df.date = df.date.apply(lambda x: x.date())
    return df
        

def store_data(df, path, filename):
    """store data given as a dataframe to the csv filename"""
    if not os.path.exists(path):
        os.mkdir(path)
    df.to_csv(path + '/' + filename, index=False)


def store_all_data(kite, start_date, end_date):
    for k,v in cfg.instruments['Aluminium_futures'].items():
        print(f"Retrieving data for {v['token']} ")
        df = get_data_from_kite(kite, v['token'], start_date, end_date)
        store_data(df, cfg.historical_data_path, f"{v['token']}.csv")


def get_instruments():
    return cfg.instruments


def get_historical_data(token):
    return pd.read_csv(cfg.historical_data_path + f'/{token}.csv')


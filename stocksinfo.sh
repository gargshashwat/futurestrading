#!/usr/bin/env bash

source env/bin/activate
python stocksinfo.py
python wheel_candidates.py
python wheel_trades.py
deactivate
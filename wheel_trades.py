import pandas as pd

# choose one strike per stock
df = pd.read_excel('wheel_candidates.xlsx', index_col=0)
df_more = df[df.Delta >= -0.3].sort_values(by=['Ticker', 'Delta']).groupby(['Ticker']).head(1)
df_less = df[df.Delta < -0.3].sort_values(by=['Ticker', 'Delta']).groupby(['Ticker']).tail(1)
df = pd.concat([df_more, df_less]).sort_values(by=['Ticker', 'Delta']).groupby('Ticker').tail(1)
df.reset_index(inplace=True, drop=True)

# remove rows with expiry after earnings date
info = pd.read_excel('stocks_info.xlsx', index_col=0)
df = df.merge(info, on='Ticker', how='left')
df = df[df.expiry < df['Earnings Date']]

# estimate IB maintenance margin
df['Est.Margin'] = 15 * df.Strike

# save file
df.to_excel('wheel_trades.xlsx')

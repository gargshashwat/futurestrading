import pandas as pd
import numpy as np
import time


from selenium import webdriver
from selenium.webdriver.chrome.options import Options 
from webdriver_manager.chrome import ChromeDriverManager 
from selenium.webdriver.support.ui import Select


"""This script only returns monthly put contracts with dte in [30,45] and delta in [-0.4, -0.2]."""


def get_zacks_options_data(ticker, previous=False):
    # go to page
    url = f'https://www.zacks.com/stock/quote/{ticker}/greeks-montage'
    options = Options()
    options.add_argument("no-sandbox")
    options.add_argument("headless")
    options.add_argument("start-maximized")
    options.add_argument("window-size=1900,1080")
    options.add_argument("user-agent=Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36")
    driver = webdriver.Chrome(ChromeDriverManager().install(), options=options)
    driver.get(url)
    
    # wait until page fully loads
    while(True):
        try:
            time.sleep(5)
            options_data = driver.find_element_by_id(f'option_montage_table_0_wrapper')
        except:
            continue
        break
        
    # iterate over table to find one for PUTS with days-till-expiration in the range [30,45]
    i = 0
    while(True):
        javaScript = "window.scrollBy(0,500);"
        driver.execute_script(javaScript)    # scroll to put the current table in centre of screen

        options_data = driver.find_element_by_id(f'option_montage_table_{i}_wrapper')
        heading = options_data.text.split('\n')[0]
        if 'PUTS' in heading:
            dte = int(heading.split('/')[1].split(' ')[1])
            if dte > 45:
                break
            if dte >= 30:
                if previous:
                    btn = options_data.find_element_by_link_text('Previous')
                    driver.execute_script("arguments[0].click();", btn)
                    options_data = driver.find_element_by_id(f'option_montage_table_{i}_wrapper')
                return options_data
        i += 1
        
    driver.close()   


def extract_trades_from_options_data(ticker, options_data):
    expiry_date = options_data.text[options_data.text.find('(')+1: options_data.text.rfind(')')]
    
    table = options_data.text[options_data.text.find('Strike'): options_data.text.rfind('Strike')]
    lines = table.split('\n')[1:]
    
    trades = []

    # detect if need to look at previous page. If yes, add those trades as well 
    delta_of_first_line = float(lines[1].split(' ')[10])
    if delta_of_first_line <= -0.2:
        options_data_prev = get_zacks_options_data(ticker, previous=True)
        di_prev = extract_trades_from_options_data(ticker, options_data_prev)
        trades = di_prev

    for strike, vals in zip(lines[::2], lines[1::2]):
        vals = vals.split(' ')
        delta = float(vals[10])
        if (delta >= -0.4) and (delta <= -0.2):

            di = {'Ticker': ticker,
                  'Strike': strike,
                  'Delta': delta,
                  'expiry': expiry_date,
                  'bid': float(vals[0]),
                  'ask': float(vals[1]),
                  'last': float(vals[2]),
                  'volume': int(vals[6].replace(',', '')),
                  'open_interest': int(vals[7].replace(',', '')),
                  'IV': float(vals[8])}

            trades.append(di)

    return trades


def get_candidates(tickers):
    print('Wheel trades')
    trades = []
    if type(tickers) is str:
        print(f'{tickers}')
        options_data = get_zacks_options_data(tickers)
        trades += extract_trades_from_options_data(tickers, options_data)
    else:
        for idx, t in enumerate(tickers):
            print(f'{idx+1}/{len(tickers)}: {t}')
            options_data = get_zacks_options_data(t)
            trades += extract_trades_from_options_data(t, options_data)
                
    df = pd.DataFrame(trades)
    df['expiry'] = pd.to_datetime(df['expiry'])
    df.to_excel('wheel_candidates.xlsx')
     

tickers = ['ATVI', 'AAPL', 'EBAY', 'XOM', 'F', 'INTC', 'PLTR', 'PTON', 'SBUX', 'TWTR', 'NTNX', 'RBLX', 'SNAP', 'AMD']
get_candidates(tickers)



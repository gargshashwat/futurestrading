import pandas as pd
import numpy as np
import time

from selenium import webdriver
from selenium.webdriver.chrome.options import Options 
from webdriver_manager.chrome import ChromeDriverManager 
from selenium.webdriver.support.ui import Select


def get_zacks_earnings_date(ticker):
    url = f'https://www.zacks.com/stock/research/{ticker}/earnings-announcements'

    options = Options()
    options.add_argument("no-sandbox")
    options.add_argument("headless")
    options.add_argument("start-maximized")
    options.add_argument("window-size=1900,1080")
    
    driver = webdriver.Chrome(ChromeDriverManager().install(), options=options)
    driver.get(url)
    
    earnings_date_table = driver.find_element_by_class_name('key-expected-earnings-data-module')
    text = earnings_date_table.text
    date = text.split('\n')[1]
    
    driver.close()
    
    return date


def get_zacks_analyst_recommendations(ticker):
    url = f'https://www.zacks.com/stock/research/{ticker}/brokerage-recommendations'

    options = Options()
    options.add_argument("no-sandbox")
    options.add_argument("headless")
    options.add_argument("start-maximized")
    options.add_argument("window-size=1900,1080")
    
    driver = webdriver.Chrome(ChromeDriverManager().install(), options=options)
    driver.get(url)
    
    reco_table = driver.find_element_by_id('current_abr')
    text = reco_table.text
    abr = float(text.split('\n')[4])
    
    zacks_rating = driver.find_element_by_class_name('quote_rank_summary')
    text = zacks_rating.text
    zacks_rating = int(text.split('\n')[4])
    
    driver.close()
    
    return abr, zacks_rating


def get_finviz_data(ticker):
    url = f'https://finviz.com/quote.ashx?t={ticker}'

    options = Options()
    options.add_argument("no-sandbox")
    options.add_argument("headless")
    options.add_argument("start-maximized")
    options.add_argument("window-size=1900,1080")
    options.add_argument("user-agent=Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36")
    
    driver = webdriver.Chrome(ChromeDriverManager().install(), options=options)
    driver.get(url)
    
    data = driver.find_element_by_class_name('snapshot-table2')
    text = data.text
    pe = text[text.find('P/E'):].split(' ')[1]
    eps = text[text.find('EPS (ttm)'):].split(' ')[2]
    close = text[text.rfind('Price'):].split(' ')[1].split('\n')[0]
    
    driver.close()
    
    return pe, eps, close


def store_earnings_date(tickers):
    print('Stocks data')
    di = {}
    for idx, t in enumerate(tickers):
        print(f'{idx+1}/{len(tickers)}: {t}')
        di[t] = {}
        di[t]['Earnings Date'] = get_zacks_earnings_date(t)
        di[t]['Average Broker Rating'], di[t]['Zacks Rating'] = get_zacks_analyst_recommendations(t)
        di[t]['P/E'], di[t]['EPS'], di[t]['Close'] = get_finviz_data(t)


    df = pd.DataFrame.from_dict(di, orient='index')
    df['Earnings Date'] = pd.to_datetime(df['Earnings Date'], errors='coerce')    
    df.reset_index(inplace=True)
    df.rename(columns={'index': 'Ticker'}, inplace=True)
    df.sort_values(by=['Ticker'], inplace=True)
    df.reset_index(drop=True, inplace=True)

    df.to_excel('stocks_info.xlsx')

    
tickers = ['GME', 'AMC', 'BB', 'IRNT', 'LCID', 'OPAD', 'GOEV', 'SOFI', 'ATVI', 'AAPL', 'EBAY', 'XOM', 
           'F', 'INTC', 'PLTR', 'PTON', 'SBUX', 'TWTR', 'NTNX', 'RBLX', 'SNAP', 'AMD']

store_earnings_date(tickers)
